#!/bin/bash
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
cd /home/vagrant/
git clone https://gitlab.com/pclemente/masterclassdevops.git
# git clone git@gitlab.com:pclemente/masterclassdevops.git
cd /home/vagrant/masterclassdevops/jenkins/
chmod 600 /home/vagrant/masterclassdevops/jenkins/home/.ssh/*
docker build . -t myregistry.local:5000/jenkins:latest
docker push myregistry.local:5000/jenkins:latest
sudo chmod 666 /var/run/docker.sock 
docker stack deploy -c docker-compose.yml jenkins